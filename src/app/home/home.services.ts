import { Injectable } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class HomeServices {


  private headers = new Headers({
    'Content-Type': 'application/json',
    'dataType': 'jsonp',
    'Access-Control-Allow-Origin': '*',
    'Authorization': ''
  });
  private url = '' ;
  private localhost = 'localhost:8000';
  constructor(private http: Http) {
  }

  getListOfNews(payload) {
    this.url = '/businessapp/controller/user/news/list';
    return this.http.post(this.url, payload ,{headers: this.headers} )
      .map((response: Response) => {
        var body = response.json();
        return body;
      }).catch(this._errorHandler);
  }
  addNews(payload, accessToken1) {
  var headers1 = new Headers({
      'Content-Type': 'application/json',
      'dataType': 'jsonp',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + accessToken1
    });
    this.url = '/businessapp/controller/user/addnews';
    return this.http.post(this.url, payload,{headers: headers1} )
      .map((response: Response) => {
        var body = response.json();
        return body;
      }).catch(this._errorHandler);
  }
  getUrlMeatdata(url) {
 var  headers1 = new Headers();
    this.url = 'https://api.urlmeta.org/?url=' + url;
    console.log("url is " + this.url)
    return this.http.get(this.url, {headers: headers1} )
      .map((response: Response) => {
        var body = response.json();
        return body;
      }).catch(this._errorHandler);
  }
  likeNews(payload,accessToken1) {
    var headers1 = new Headers({
      'Content-Type': 'application/json',
      'dataType': 'jsonp',
      'Access-Control-Allow-Origin': '*',
      'Authorization': 'bearer ' + accessToken1
    });
    this.url = '/businessapp/controller/user/news/like';
    return this.http.post(this.url, payload,{headers: headers1} )
      .map((response: Response) => {
        var body = response.json();
        return body;
      }).catch(this._errorHandler);
  }
  _errorHandler(error: Response) {
    console.error(error);
    return Observable.throw(error || 'server Error');

  }

}

