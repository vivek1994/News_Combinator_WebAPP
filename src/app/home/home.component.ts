import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import {HomeServices} from './home.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [HomeServices]
})
export class HomeComponent implements OnInit {
  listOfNews = [];
  checkurl = true;
  addNew = true;
  listNews = false;
  hideTitle = true;
  title = '';
  splitedData
  url = '';
  text = '';
  accessToken = null;
  userInfo
  skip = 0;
  isMoreList;
  constructor(private homeServices: HomeServices, private router: Router) {
    this.getListOFNews();
  }

  ngOnInit() {
    console.log("bbbbbbb");
  }
  Addnew() {
    this.listNews = true;
    this.addNew = false;
    console.log("ranjan");
  }

  getUrlDetails() {
    this.splitedData = this.url.split('www.')
    this.homeServices.getUrlMeatdata(this.url).subscribe(
      response => {
        console.log("login" + JSON.stringify(response));
        if (response.result.status === 'OK') {
          this.text = response.meta.description;
          this.title = this.splitedData[1] + ':' + response.meta.title;
          this.checkurl = false;
          this.hideTitle = false;
        }
      },
      error => {
        var text = JSON.parse((error.text()));
        this.checkurl = false;
        this.hideTitle = false;
      });
  }

  AddNews() {
    this.accessToken = localStorage.getItem("accessToken")
    console.log("accessToken111111 is ", +this.accessToken);
    if (this.url === '' || this.title === '') {
      return;
    }
    var payload = {
      'url': this.url,
      'text': this.text,
      'title': this.title,
    }
    console.log("accessToken is", +this.accessToken)
    this.homeServices.addNews(payload, this.accessToken).subscribe(
      response => {
        console.log("login" + JSON.stringify(response));
        this.getListOFNews();
      },
      error => {
        var text = JSON.parse((error.text()));
        this.checkurl = false;
        this.hideTitle = false;
      });
  }

  getListOFNews() {
    this.accessToken = localStorage.getItem('accessToken')
    console.log("accessToken is" + this.accessToken)
    var payload = {
      '_id': this.accessToken,
      'skip': this.skip
    };
    this.homeServices.getListOfNews(payload)
      .subscribe(
        response => {
          console.log("login" + JSON.stringify(response));
          this.isMoreList = response.IsMoreList
          for (var i = 0; i < response.listOfNews.length; i++) {
            this.listOfNews.push(response.listOfNews[i]);
          }
          this.listNews = false;
          this.addNew = true;
          this.skip = this.skip + 1;
        },
        error => {
          var text = JSON.parse((error.text()));
        });
  }

  cancel() {
    this.getListOFNews();
  }


  likeNews(index) {
    if (localStorage.getItem('accessToken') === null || localStorage.getItem('accessToken') === 'undefined') {
      this.router.navigate(['signup']);
    } else {
      this.accessToken = localStorage.getItem('accessToken')
    }
    console.log("likeNews");
    this.listOfNews[index].likeFlag = true;
    this.likedService(index);
}
  UnlikeNews(index) {
    this.listOfNews[index].likeFlag = false;
    if (localStorage.getItem('accessToken') === null || localStorage.getItem('accessToken') === 'undefined') {
      this.router.navigate(['signup']);
    } else {
      this.accessToken = localStorage.getItem('accessToken');
    }
    this.likedService(index);
  }

  likedService(index) {
    var payload = {
      'newsId': this.listOfNews[index]._id,
    };
    this.homeServices.likeNews(payload, this.accessToken)
      .subscribe(
        response => {
          console.log("login" + JSON.stringify(response));
          this.getListOFNews();
        },
        error => {
          var text = JSON.parse((error.text()));
        });
  }
}
