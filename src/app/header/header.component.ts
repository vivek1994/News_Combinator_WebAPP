import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  showData : boolean = true;
  hideData : boolean = false;
  userName:string = " ";
  @Output() myEvent = new EventEmitter();
  constructor(private router: Router) {
    if (localStorage.getItem('accessToken') !== null && localStorage.getItem('accessToken') !== 'undefined') {
      this.redirectPage();
    }
  }

  ngOnInit() {
  }
  redirectPage() {
    this.userName = localStorage.getItem('name')
    this.showData = false;
    this. hideData = true;
    this.router.navigate(['home']);
  }
  logOut() {
    localStorage.clear();
    this.showData = true;
    this. hideData = false;
    localStorage.setItem("check","sign");
    this.router.navigate(['signup']);
  }
  newLink() {
    this.myEvent.emit(null);
  }
}
