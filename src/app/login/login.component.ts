import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {LoginServicesData} from './login.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [LoginServicesData]
})
export class LoginComponent implements OnInit {
mobile = '';
pass = '';
inavlid= '';
loginError='';

@Output() showUserDetail: EventEmitter<string> = new EventEmitter<string>();

  constructor(private getData: LoginServicesData, private router: Router) { }
  ngOnInit() {
  }

submit() {
  this.inavlid = '';
    console.log('vivekkkkkc        ' + this.mobile);
    console.log(this.pass);
    if (this.mobile === '' || this.pass === '') {
      this.inavlid = "field can't be blank";
      return this.inavlid;
    }

  var loginPayload={
    'mobile' : this.mobile,
    'password' : this.pass,
  }
  this.getData.getMenuData(loginPayload)
    .subscribe(
      response => {
        console.log("login"+JSON.stringify(response));
         this.router.navigate(['home']);
        localStorage.setItem('accessToken', response.userinfo._id );
        localStorage.setItem('name', response.userinfo.name );
        console.log("vipin");
      },
      error => {
        var text = JSON.parse((error.text()));
        this.loginError = text.message;
      });
}
cancel() {
  this.router.navigate(['home']);
}
}
