import { Component, OnInit } from '@angular/core';
import {SignupServices} from './signup.services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  providers: [SignupServices]
})
export class SignupComponent implements OnInit {
mobile = '';
name = '';
password = '';
  constructor(private signupService: SignupServices, private router: Router) { }

  ngOnInit() {

    var check = localStorage.getItem("check");
    if(check == "sign"){
      localStorage.removeItem("check");
      this.router.navigate(['home']);
      return;
    }

  }
   signup() {
  var signupPayload={
    'name': this.name,
    'mobile' : this.mobile,
    'password' : this.password,
  }
  this.signupService.signup(signupPayload)
    .subscribe(
      response => {
        console.log("login "+JSON.stringify(response));
        this.router.navigate(['home']);
      },
      error => {
        var text = JSON.parse((error.text()));
      });
}
cancel() {
    this.router.navigate(['home']);
}
}
